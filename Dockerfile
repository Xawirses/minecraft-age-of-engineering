FROM ironcraft/minecraft-forge:1.10.2
MAINTAINER Xawirses <xawirses@gmail.com>
LABEL maintainer="Xawirses <xawirses@gmail.com>"

RUN apt update && apt install -y unzip zip wget python

ENV AOE_VERSION 1.0.4
ENV AOE_URL https://addons-origin.cursecdn.com/files/2455/50/Age%20of%20Engineering-${AOE_VERSION}.zip

RUN curl --create-dirs -sLo /build/aoe.zip "${AOE_URL}" && \
  cd /build && \
  unzip aoe.zip -d /build/aoe && \
  cp -Rfv /build/aoe/overrides/* /minecraft/

COPY dl_mods.sh /build/dl_mods.sh
RUN chmod +x /build/dl_mods.sh && bash /build/dl_mods.sh
RUN rm -Rf /build

EXPOSE 25565

ENTRYPOINT ["/usr/bin/entrypoint.sh"]