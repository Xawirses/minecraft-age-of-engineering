# Minecraft

[![](https://images.microbadger.com/badges/image/ironcraft/minecraft-age-of-engineering.svg)](https://microbadger.com/images/ironcraft/minecraft-age-of-engineering 
"Get your own image badge on microbadger.com")

These are docker images for [Age of Engineering](https://mods.curse.com/modpacks/minecraft/263897-age-of-engineering) [Minecraft](https://minecraft.net) Modpack power by [Minecraft Forge](http://www.minecraftforge.net/).

## Ports
* 25565

## Available environment variables

```bash
ENV JAVA_RAM 4G
ENV JAVA_OPTS -XX:+UseG1GC -Xmx${JAVA_RAM} -Xms${JAVA_RAM} -Dsun.rmi.dgc.server.gcInterval=2147483646 -XX:+UnlockExperimentalVMOptions -XX:G1NewSizePercent=20 -XX:G1ReservePercent=20 -XX:MaxGCPauseMillis=50 -XX:G1HeapRegionSize=32M
```

## Contributing

Fork -> Patch -> Push -> Pull Request

## Authors

* [Xawirses](https://bitbucket.org/Xawirses/)