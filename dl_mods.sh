#!/bin/bash

function getJsonVal() { 
    python -c "import json,sys;sys.stdout.write(json.dumps(json.load(sys.stdin)$1))"; 
}

IFS=';' 

files=$(cat /build/aoe/manifest.json | getJsonVal "['files']")
files=${files//'}, {'/';'}
files=${files//'[{'/''}
files=${files//'}]'/''}

for addr in $files
do
	echo $addr
	p=$(echo '{'$addr'}' | getJsonVal "['projectID']")
	f=$(echo '{'$addr'}' | getJsonVal "['fileID']")

	if [ -n "$p" ]; then
		wget https://minecraft.curseforge.com/projects/$p/files/$f/download --content-disposition -P /minecraft/mods/
	fi
done
